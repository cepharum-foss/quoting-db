# Shell quoting DB

A collection of JSON-encoded configurations for escaping arguments on invoking shells.

## License

[MIT](LICENSE)

## About

This package has been created to document and consider different requirements of existing shells for properly escaping arguments to prevent unintended side effects e.g. due to passing functional characters. 

All configurations in this package result from testing different ways of escaping every special character in ASCII code range with and without wrapping quotes. The resulting records can be read directly from a JSON file or via some convenience helper method.

### Testing procedure

Tests are performed by two scripts: **packages/library/runner.js** and **packages/library/logger.js**. The former is invoking the latter as a sub-process via some shell. **logger.js** is displaying codepoints of all characters received as argument. **runner.js** is then assessing this output to recognize whether some provided argument has been properly received by **logger.js**.

This basic procedure is repeated many times 

* to use different characters, each
* optionally combined with other characters, 
* optionally using different ways of escaping and 
* optionally wrapping the argument in single or double quotes. 

The result of every test is computed to eventually create some JSON-encoded configuration file for the tested shell.

Shells on `linux` platform have been tested in a Debian-based Docker container. Windows' `cmd.exe` and `powershell.exe` have been tested on a Windows 11-based device.

### Running tests

You can invoke the tests on your local machine with

```bash
npm run start
```

This will generate lots of information regarding encountered issues you may ignore on stderr. Eventually, it provides a JSON-encoded configuration for your system's default shell on stdout. 

If you want the configuration to be written to local library's database of configurations, use this command instead:

```bash
npm run write
```

Either command accepts one or more names of shells to test instead of the system's default. For example, on Windows you could test PowerShell instead of CMD with

```bash
npm run start powershell
```

or 

```bash
npm run write powershell
```

In addition, there is docker composition setting up docker container based on LTS version of Node in a Debian-based environment for testing a selection of available shells it is installing explicitly. Results are written to the local database folder, too.

```bash
npm run write:docker
```

### Tested characters

Tests cover all whitespace characters and special characters of ASCII code page.

## Configurations

For every tested shell there is a JSON file describing test results prepared for quoting and escaping arguments when using either shell. Every file's data is divided into separate configurations per character optionally used to wrap an argument. This character is either the empty string for using no enclosing quotes or `"` or `'` for using either character for enclosing the argument. A configuration for a quoting character may be missing in case the tested shell renders incapable of handling accordingly wrapped arguments.

Either configuration per quoting character consists of these properties:

- `mapPattern` is a string containing a regular expression matching any character that needs to be escaped.
- `map` is mapping every character requiring to be escaped into either one's escape sequence.
- `rejectPattern` is a string providing another regular expression matching characters that can't be escaped at all. Arguments matching this pattern should be rejected unless there is a different configuration working for them.
- `invalid` is mapping characters that can't be escaped into `true` for simplified lookups.
- `prevent` is map primarily for information purposes. It marks characters that should not be escaped with a preceding backslash or caret as those combinations will be visible to the invoked script. When using patterns as described before, it is safe to ignore this map, though.

## Usage

See [the official readme of the package](packages/library/readme.md) for details on how to use it.
