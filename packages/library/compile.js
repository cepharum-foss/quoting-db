const File = require( "node:fs" );
const Path = require( "node:path" );

( async function() {
	const dbFolder = Path.resolve( __dirname, "db" );

	const files = await File.promises.readdir( dbFolder );
	const collection = {};

	for ( const file of files ) {
		if ( /[a-z]\.json$/.test( file ) ) {
			const rawContent = await File.promises.readFile( Path.resolve( dbFolder, file ), { encoding: "utf-8" } ); // eslint-disable-line no-await-in-loop
			const configuration = JSON.parse( rawContent );
			const reduced = {};

			for ( const quote of [ "", "'", '"' ] ) {
				const source = configuration[quote];

				if ( source != null ) {
					reduced[quote] = {
						mapPattern: source.mapPattern,
						map: source.map,
						rejectPattern: source.rejectPattern,
					};
				}
			}

			collection[configuration.shell] = reduced;

			const alias = configuration.shell.replace( /\.exe$/, "" );
			if ( alias !== configuration.shell ) {
				collection[alias] = reduced;
			}
		}
	}

	await File.promises.writeFile( Path.resolve( __dirname, "configurations.js" ), `module.exports = ${JSON.stringify( collection, undefined, "\t" )};\n` );
} )();
