if ( typeof process.argv[2] !== "string" ) {
	console.error( "no argument to process" );
	process.exit( 2 );
}

console.log( process.argv[2]
	.split( "" )
	.map( character => character.codePointAt( 0 ).toString( 16 ).padStart( 2, "0" ) )
	.join( " " )
);
