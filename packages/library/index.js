const Path = require( "node:path" );
const File = require( "node:fs" );

const configurationPerShell = require( "./configurations.js" );

/**
 * Derives name of used shell from provided value to be used with option `shell`
 * of `child_process.spawn()`.
 *
 * @param {string|true} [shell] value for use with `child_process.shell`
 * @returns {string} name of shell to be used by `child_process.shell`
 */
const getShellName = shell => {
	let name;

	if ( shell == null || shell === true ) {
		name = Path.basename( process.platform === "win32" ? process.env.ComSpec : "/bin/sh" );
	} else {
		name = Path.basename( shell );
	}

	name = name.toLowerCase();

	switch ( name ) {
		case "powershell" :
			return "powershell.exe";

		case "cmd" :
			return "cmd.exe";

		default :
			return name;
	}
};

/**
 * Loads configuration for quoting arguments prior to invoking named shell (or
 * default shell on providing true or omitting argument here).
 *
 * @param {string|true} [shell] value for use with `child_process.shell`
 * @returns {Promise<Configuration>} promise for found configuration of selected shell
 */
const getShellConfiguration = async shell => {
	const name = getShellName( shell );

	const filename = Path.resolve( __dirname, "db", name + ".json" );
	const code = await File.promises.readFile( filename, { encoding: "utf-8" } );

	return JSON.parse( code );
};

/**
 * Synchronously loads configuration for quoting arguments prior to invoking
 * named shell (or default shell on providing true or omitting argument here).
 *
 * @param {string|true} [shell] value for use with `child_process.shell`
 * @returns {CoreConfiguration} found configuration of selected shell, undefined on missing configuration for selected shell
 */
const getShellConfigurationSync = shell => configurationPerShell[getShellName( shell )];

/**
 * Escapes special characters in provided argument based on provided
 * configuration.
 *
 * @param {string} argument single (!!) argument to be passed to some script
 * @param {Configuration} configuration configuration of shell invoked to run script
 * @returns {string} provided argument prepared for passing to some script
 */
const quote = ( argument, configuration ) => {
	for ( const q of [ "", "'", '"' ] ) {
		if ( configuration[q] ) {
			const { rejectPattern, mapPattern, map } = configuration[q];

			if ( !new RegExp( rejectPattern ).test( argument ) ) {
				return q + String( argument ).replace( new RegExp( mapPattern, "g" ), c => map[c] ) + q;
			}
		}
	}

	throw new TypeError( `argument can't be prepared for safely passing to some script run with ${configuration.shell}` );
};

exports.configurationPerShell = configurationPerShell;
exports.getShellName = getShellName;
exports.getShellConfiguration = getShellConfiguration;
exports.getShellConfigurationSync = getShellConfigurationSync;
exports.quote = quote;
