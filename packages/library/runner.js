/* eslint-disable no-await-in-loop */
const Path = require( "node:path" );
const Child = require( "node:child_process" );
const File = require( "node:fs" );

const candidates = allAsciiPrintables().replace( /[a-z0-9]/ig, "" );
const regExpEscapes = {
	"]": "\\]",
	"\\": "\\\\",
};

const dashAtEnd = ( l, r ) => ( l === "-" ? 1 : r === "-" ? -1 : 0 );

const info = text => text.split( /\r?\n/ ).map( line => "INFO: " + line ).join( "\n" );

( async() => {
	const formats = [ "_", "_a", "a_", "a_a" ];

	const shells = process.argv.slice( 2 );
	if ( shells.length === 0 ) {
		shells.push( true );
	}

	for ( const shell of shells ) {
		const results = {
			"": {
				map: {},
				prevent: {},
				invalid: {},
			},
			"'": {
				map: {},
				prevent: {},
				invalid: {},
			},
			'"': {
				map: {},
				prevent: {},
				invalid: {},
			},
		};

		for ( let i = 0; i < candidates.length; i++ ) {
			const character = candidates.charAt( i );
			const code = candidates.charCodeAt( i );

			process.stderr.write( "testing " + character + ` (${i + 1}/${candidates.length})\r` );

			for ( const [ quote, { map, prevent, invalid } ] of Object.entries( results ) ) {
				for ( const format of formats ) {
					const [ issueRaw, issueBackslash, issueCaret, issueDoubling ] = await Promise.all( [
						tryCharacter( shell, character, code, character, format, quote ),
						tryCharacter( shell, character, code, `\\${character}`, format, quote ),
						tryCharacter( shell, character, code, `^${character}`, format, quote ),
						tryCharacter( shell, character, code, `${character}${character}`, format, quote ),
					] );

					if ( issueRaw ) {
						if ( issueBackslash ) {
							if ( issueCaret ) { // eslint-disable-line max-depth
								if ( issueDoubling ) { // eslint-disable-line max-depth
									console.error( info( `escaping '${format.replace( /_/g, character )}' (${code})${quote ? ` wrapped in ${quote}` : ""} failed\n  - raw: ${issueRaw}\n  - backslash: ${issueBackslash}\n  - caret: ${issueCaret}\n  - doubling: ${issueDoubling}` ) );
									invalid[character] = true;
								} else {
									map[character] = character + character;
								}
							} else {
								map[character] = `^${character}`;
							}
						} else {
							map[character] = `\\${character}`;
						}
					} else {
						if ( issueBackslash ) {
							( prevent[character] ??= {} ).backslash = true;
						}

						if ( issueCaret ) {
							( prevent[character] ??= {} ).caret = true;
						}
					}
				}

				results[quote].mapPattern = "[" + Object.keys( map ).sort( dashAtEnd ).map( c => regExpEscapes[c] ?? c ).join( "" ) + "]";
				results[quote].rejectPattern = "[" + Object.keys( invalid ).sort( dashAtEnd ).map( c => regExpEscapes[c] ?? c ).join( "" ) + "]";
			}
		}

		process.stderr.write( "\n" );

		for ( const [ quote, { invalid } ] of Object.entries( results ) ) {
			if ( Object.keys( invalid ).length === candidates.length ) {
				delete results[quote];
			}
		}

		const shellName = Path.basename( shell == null || shell === true ? process.platform === "win32" ? process.env.ComSpec : "/bin/sh" : shell );

		// https://jsperf.app/bofisi
		const config = JSON.stringify( {
			version: 1,
			build: new Date().toUTCString(),
			platform: process.platform,
			shell: shellName,
			...results,
		}, null, 4 );

		if ( process.env.OUTPUT_FOLDER ) {
			await File.promises.mkdir( process.env.OUTPUT_FOLDER, { recursive: true } );
			await File.promises.writeFile( Path.resolve( process.env.OUTPUT_FOLDER, shellName + ".json" ), config, { encoding: "utf-8" } );
		} else {
			console.log( config );
		}
	}
} )();

/**
 * Spawns sub-process and assesses its output displaying codepoints of received
 * argument.
 *
 * @param {string|true} shell path name of shell to use explicitly, true for using platform's default shell
 * @param {string} character character to test for supporting/requiring escaping
 * @param {number} code provided character's ASCII code
 * @param {string} encoded optional escaping of character to test
 * @param {string} format pattern to use for optionally embedded encoded character in context of additional characters
 * @param {""|"'"|'"'} quote quotes to use for wrapping argument passed to sub-process
 * @returns {Promise<undefined|string>} promise settled with optional description of an encountered issue after sub-process has finished
 */
async function tryCharacter( shell, character, code, encoded, format, quote ) {
	try {
		const result = await new Promise( ( resolve, reject ) => {
			const sub = Child.spawn( "node", [ "logger.js", quote + format.replace( /_/g, encoded ) + quote ], {
				shell: shell || true
			} );

			const stdout = [];
			const stderr = [];
			let done = 0;

			const checkDone = state => {
				if ( state === 7 ) {
					resolve( {
						code: sub.exitCode,
						stdout: Buffer.concat( stdout ).toString( "utf8" ),
						stderr: Buffer.concat( stderr ).toString( "utf8" ),
					} );
				}
			};

			sub.stdout.on( "data", chunk => stdout.push( chunk ) );
			sub.stdout.once( "end", () => checkDone( done |= 1 ) );
			sub.stderr.on( "data", chunk => stderr.push( chunk ) );
			sub.stderr.once( "end", () => checkDone( done |= 2 ) );

			sub.once( "exit", () => checkDone( done |= 4 ) );

			sub.once( "error", reject );
		} );

		if ( result.code !== 0 ) {
			return `${character} (${code}) -> exit code ${result.code} (stdout: ${result.stdout.trim()}, stderr: ${result.stderr.trim()})`;
		}

		const pattern = new RegExp( "^" + format.split( "" ).map( v => ( v === "a" ? "61" : "([\\da-f]{2})" ) ).join( " " ) + "\r?\n$", "i" );

		if ( !pattern.test( result.stdout ) ) {
			return `${character} (${code}) -> unexpected output (stdout: ${result.stdout.trim()})`;
		}

		const extract = result.stdout.replace( pattern, "$1" );

		if ( parseInt( extract, 16 ) !== code ) {
			return `${character} (${code}) -> unexpected codepoint (stdout: ${result.stdout.trim()})`;
		}

		return undefined;
	} catch ( cause ) {
		return `${character} (${code}) -> test run failed: ${cause.message}`;
	}
}

/**
 * Generates a string with all ASCII characters that are expected to potentially
 * require escaping when used in an argument to some sub-process invoked via
 * shell.
 *
 * @returns {string} all characters that might need escaping
 */
function allAsciiPrintables() {
	let result = "\t\r\n\f";

	for ( let i = 32; i < 127; i++ ) {
		result += String.fromCharCode( i );
	}

	return result;
}
