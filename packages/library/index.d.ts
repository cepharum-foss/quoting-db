declare module "@cepharum/quoting-db" {
	/**
	 * Derives name of shell used to invoke some script from provided value used
	 * with option `shell` of Node's `spawn()` method.
	 *
	 * @param shell name of shell to use explicitly, true to select current platform's default shell
	 * @returns normalized name of selected shell
	 */
	export function getShellName( shell?: string|true ): string;

	/**
	 * Fetches configuration of selected shell.
	 *
	 * The provided shell selector is the value used as option `shell` on
	 * invoking a script using Node's `spawn()` method.
	 *
	 * @param shell name of shell to use explicitly, true to select current platform's default shell
	 * @returns promise for selected shell's full configuration
	 * @throws if there is no configuration for selected shell
	 */
	export function getShellConfiguration( shell?: string|true ): Promise<Configuration>;

	/**
	 * Synchronously fetches configuration necessary for quoting characters for
	 * selected shell.
	 *
	 * The provided shell selector is the value used as option `shell` on
	 * invoking a script using Node's `spawn()` method.
	 *
	 * @param shell name of shell to use explicitly, true to select current platform's default shell
	 * @returns selected shell's basic configuration essential for quoting arguments
	 * @throws if there is no configuration for selected shell
	 */
	export function getShellConfigurationSync( shell?: string|true ): CoreConfiguration;

	/**
	 * Adjusts provided argument for safe use on invoking sub-process via some
	 * shell.
	 *
	 * @param argument argument to be provided on invoking some script
	 * @param configuration configuration of shell used to run the invoked script
	 * @returns provided argument prepared for passing
	 */
	export function quote( argument: string, configuration: Configuration|CoreConfiguration ): string;

	/**
	 * Provides a compiled collection of all available configurations per shell
	 * focusing on information necessary for quoting arguments.
	 */
	export const configurationPerShell: { [shellName: string]: CoreConfiguration };

	/**
	 * Describes configuration of a single shell.
	 */
	export interface CoreConfiguration {
		/**
		 * Provides configuration for escaping arguments not enclosed by quotes.
		 *
		 * If this configuration is missing, wrapping arguments in quotes is mandatory.
		 */
		""?: CoreConfigurationPerQuoting;

		/**
		 * Provides configuration for escaping arguments that have been enclosed
		 * by single quotes.
		 *
		 * If this configuration is missing, wrapping arguments in single quotes
		 * is commonly causing issues with the shell and should be avoided.
		 */
		"'"?: CoreConfigurationPerQuoting;

		/**
		 * Provides configuration for escaping arguments that have been enclosed
		 * by single quotes.
		 *
		 * If this configuration is missing, wrapping arguments in double quotes
		 * is commonly causing issues with the shell and should be avoided.
		 */
		'"'?: CoreConfigurationPerQuoting;
	}

	/**
	 * Describes configuration of a single shell.
	 */
	export interface Configuration {
		/**
		 * Indicates version of this shell's configuration.
		 *
		 * This value is used to prepare for future upgrades of this database
		 * causing breaking changes to the configuration's structure.
		 */
		version: number;

		/**
		 * Provides date and time this record has been created.
		 */
		build: string;

		/**
		 * Provides platform the record has been created on.
		 */
		platform: string;

		/**
		 * Names shell this configuration applies to (on named platform).
		 */
		shell: string;

		/**
		 * Provides configuration for escaping arguments not enclosed by quotes.
		 *
		 * If this configuration is missing, wrapping arguments in quotes is mandatory.
		 */
		""?: ConfigurationPerQuoting;

		/**
		 * Provides configuration for escaping arguments that have been enclosed
		 * by single quotes.
		 *
		 * If this configuration is missing, wrapping arguments in single quotes
		 * is commonly causing issues with the shell and should be avoided.
		 */
		"'"?: ConfigurationPerQuoting;

		/**
		 * Provides configuration for escaping arguments that have been enclosed
		 * by single quotes.
		 *
		 * If this configuration is missing, wrapping arguments in double quotes
		 * is commonly causing issues with the shell and should be avoided.
		 */
		'"'?: ConfigurationPerQuoting;
	}

	/**
	 * Describes configuration for escaping characters.
	 */
	export interface CoreConfigurationPerQuoting {
		/**
		 * Provides a regular expression matching all characters that need to be
		 * escaped.
		 */
		mapPattern: string;

		/**
		 * Maps characters to be escaped into their proper escaping sequence.
		 */
		map: { [character: string]: string };

		/**
		 * Provides a regular expression matching all characters that do not
		 * work with any known escaping pattern and thus should be rejected when
		 * encountered in an argument.
		 */
		rejectPattern: string;
	}

	/**
	 * Describes configuration for escaping characters.
	 */
	export interface ConfigurationPerQuoting extends CoreConfigurationPerQuoting {
		/**
		 * Maps characters that can't be escaped into `true` for easy lookup.
		 */
		invalid: { [character: string]: true };

		/**
		 * Maps characters that should not be escaped with either a preceding
		 * backslash or a caret as those escape sequences will be passed
		 * literally.
		 */
		prevent: { [character: string]: {
			backslash?: true;
			caret?: true;
		} };
	}
}
