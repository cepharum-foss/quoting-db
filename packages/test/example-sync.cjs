const { spawn } = require( "node:child_process" );
const { getShellConfigurationSync, quote } = require( "@cepharum/quoting-db" );

const shell = process.argv[2] || true;

const invoke = ( script, args ) => {
	const configuration = getShellConfigurationSync( shell );

	return new Promise( ( resolve, reject ) => {
		const child = spawn( script, args.map( arg => quote( arg, configuration ) ), {
			shell,
		} );

		child.once( "error", reject );

		const stdout = [];
		const stderr = [];
		let done = 0;

		const checkDone = state => {
			if ( state === 7 ) {
				resolve( {
					code: child.exitCode,
					stdout: Buffer.concat( stdout ).toString( "utf-8" ),
					stderr: Buffer.concat( stderr ).toString( "utf-8" ),
				} );
			}
		};

		child.stdout.on( "data", chunk => stdout.push( chunk ) );
		child.stdout.once( "end", () => checkDone( done |= 1 ) );

		child.stderr.on( "data", chunk => stderr.push( chunk ) );
		child.stderr.once( "end", () => checkDone( done |= 2 ) );

		child.once( "exit", () => checkDone( done |= 4 ) );
	} );
};

const testString = "abc !#&$*+-_=?%|<>^° ABC";

console.log( "sent " + testString );

invoke( "node", [ "mirror.js", testString ] )
	.then( ( { code, stdout, stderr } ) => {
		if ( code !== 0 ) {
			throw new Error( `sub-process exited on ${code}` );
		}

		if ( stdout.trim() !== testString ) {
			throw new Error( `sub-process did not receive argument as provided: ${stdout.trim()}` );
		}

		if ( stderr.trim() ) {
			throw new Error( `sub-process has output on stderr: ${stderr.trim()}` );
		}

		console.log( "rcvd " + stdout.trim() );
	} )
	.catch( cause => {
		console.error( cause );
		process.exit( 2 );
	} );
